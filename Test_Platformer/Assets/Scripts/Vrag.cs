﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vrag : MonoBehaviour
{
    public int health = 1;
    public float speed;
    public static Vrag Instance { get; set; }

    private void Awake()
    {
        Instance = this;
    }

        void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);

        if (health <= 0)
        {

            Destroy(gameObject);
        }
    }

    public void TakeDamage()
    {
        health -= 1;
    }
}
