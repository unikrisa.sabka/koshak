﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healse : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == HeroTransform.Instance.gameObject)
        {
            HeroTransform.Instance.GetHeals();
            Destroy(gameObject);
        }
    }
}
