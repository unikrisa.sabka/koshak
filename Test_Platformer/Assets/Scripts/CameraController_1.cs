﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController_1 : MonoBehaviour
{
    public Transform player;
    private Vector3 pos;


    private void Awake()
    {
        if (player == null)
        {
            player = GameObject.FindWithTag("Player").transform;
        }
    }
    private void Update()
    {
        pos = player.position;
        pos.z = -10f;
        transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime);
    }
}

