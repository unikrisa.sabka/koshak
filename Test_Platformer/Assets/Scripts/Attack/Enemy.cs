﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float timeBtwAttack;
    public float startTimeBtwAttack;

    public int hp1;
    public float speed;
    public GameObject deathEffect;
    public int damage;
    private float stopTime;
    public float startStopTime;
    public float normalSpeed;
    private HeroTransform player;
    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
        player = FindObjectOfType<HeroTransform>();
        normalSpeed = speed;
    }
     
    private void Update()
    {
        if (stopTime <= 0)
        {
            speed = normalSpeed;
        }
        else
        {
            speed = 0;
            stopTime -= Time.deltaTime;
        }
        if (hp1 <= 0)
        {
            anim.Play("Death");
           // Instantiate(deathEffect, transform.position, Quaternion.identity);
           
        }
        transform.Translate(Vector2.left * speed * Time.deltaTime);
    }


    public void OnTriggerStay2D(Collider2D other)
    {
         if (other.CompareTag("Player"))
        {
            if (timeBtwAttack <= 0)
            {
                anim.SetTrigger("attack");
            }
            else
            {
                timeBtwAttack -= Time.deltaTime;
            }
        }
    }

    public void EnemyAttack()
    {
        timeBtwAttack = startStopTime;
        Instantiate(deathEffect, player.transform.position, Quaternion.identity);
        player.hp -= damage;
    }
        public void TakeDamage(int damage)
    {
        stopTime = startStopTime;
        
        hp1 -= damage;
    }
    public void Death()
    {
        Destroy(gameObject);
    }
}
