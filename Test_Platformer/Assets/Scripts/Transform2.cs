﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transform2 : MonoBehaviour
{
    public float speed = 3f;
    public float jumpForce = 10f;
    private bool isGrounded = false;



    private Rigidbody2D rb;
    private SpriteRenderer sprite;
    private Animator anim;

    private void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    private void Run()
    {
        Vector3 dir = transform.right * Input.GetAxis("Horizontal");
        transform.position = Vector3.MoveTowards(transform.position, transform.position + dir, speed * Time.deltaTime);
        sprite.flipX = dir.x > 0.0f;
    }
    private void Update()
    {
        if (Input.GetButton("Horizontal"))
            Run();
        if (Input.GetKeyDown("space"))
        {
            anim.SetTrigger("Jump");
        }
    }
}

